import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import { movieData } from '../../data/MovieData';
import { ShowMovie } from '../components/MovieComponent';
import { useEffect, useState } from 'react';
import { Icon } from 'react-native-elements';
import { Buttoncomponent } from '../components/ButtonComponent';
import DetailMovieScreen from './DetailMovieScreen';

const HomeScreen = (props) => {
  const { navigation } = props;
  const [recommended, setRecommended] = useState([]);
  const [mostViewed, setMostViewed] = useState([]);
  const [allMostViewed, setAllMostViewed] = useState([]);
  const [allRecommended, setAllRecommended] = useState([]);

  const compareRating = (a, b) => {
    const ratingA = a.rating;
    const ratingB = b.rating;

    if (ratingA > ratingB) {
      return -1;
    } else if (ratingA < ratingB) {
      return 1;
    } else {
      return 0;
    }
  };

  const compareViewers = (a, b) => {
    const viewersA = a.viewers;
    const viewersB = b.viewers;

    if (viewersA > viewersB) {
      return -1;
    } else if (viewersA < viewersB) {
      return 1;
    } else {
      return 0;
    }
  };

  useEffect(() => {
    const threeRecommended = [];
    const threeMostViewed = [];
    const sortedRecommended = [...movieData].sort(compareRating);
    setRecommended(sortedRecommended);
    const sortedMostViewed = [...movieData].sort(compareViewers);
    setMostViewed(sortedMostViewed);
    setAllMostViewed(sortedMostViewed);
    setAllRecommended(sortedRecommended);

    for (let i = 0; i < 3; i++) {
      threeRecommended.push(sortedRecommended[i]);
    }
    setRecommended(threeRecommended);

    for (let i = 0; i < 3; i++) {
      threeMostViewed.push(sortedMostViewed[i]);
    }
    setMostViewed(threeMostViewed);
  }, []);
  return (
    <View style={styles.mainContainer}>
      <FlatList
        data={recommended}
        keyExtractor={item => item.id}
        contentContainerStyle={styles.flatListContainer}
        renderItem={({ item }) => {
          return (
            <View style={styles.dataContainer}>
              <Image style={styles.movieImage} source={{ uri: item.imageLink }} />
              <View style={styles.movieDescriptionContainer}>
                <Text style={styles.title}>{item.title}</Text>
                <View style={styles.yearContainer}>
                  <Text>
                    <Icon
                      name="calendar"
                      type="antdesign"
                      size={14}
                      color="grey"
                    />{' '}
                    {item.year}
                  </Text>
                </View>
                {/* <Text>{item.rating}</Text> */}
                {item.rating === 5 ? (
                  <Image
                    style={{
                      width: 100,
                      height: 20,
                    }}
                    source={require('../../assets/images/five-stars.png')}
                  />
                ) : item.rating === 4 ? (
                  <Image
                    style={{
                      width: 100,
                      height: 20,
                    }}
                    source={require('../../assets/images/four-stars.png')}
                  />
                ) : item.rating === 3 ? (
                  <Image
                    style={{
                      width: 100,
                      height: 20,
                    }}
                    source={require('../../assets/images/three-stars.png')}
                  />
                ) : item.rating === 2 ? (
                  <Image
                    style={{
                      width: 100,
                      height: 20,
                    }}
                    source={require('../../assets/images/two-stars.png')}
                  />
                ) : (
                  <Image
                    style={{
                      width: 100,
                      height: 20,
                    }}
                    source={require('../../assets/images/one-stars.png')}
                  />
                )}
                <Buttoncomponent
                  onPress={() => navigation.navigate('DetailMovie', { item })}
                // onPress={() => navigation.navigate('DetailMovie', {title: item.title, year: item.year})}
                />
              </View>
            </View>
          );
        }}
        ListHeaderComponent={
          <View>
            <View style={styles.mainCategoryContainer}>
              <View style={styles.categoryContainer}>
                <Text style={styles.categoryText}>Most Viewed</Text>
              </View>
              <View style={styles.seeAllContainer}>
                <TouchableOpacity
                  onPress={
                    () => navigation.navigate('MostViewed', { allMostViewed })
                  }
                >
                  <Text style={styles.seeAllText}>See All</Text>
                </TouchableOpacity>
              </View>
            </View>
            <FlatList
              horizontal
              data={mostViewed}
              keyExtractor={item => item.id}
              renderItem={({ item }) => {
                return (
                  <ShowMovie
                    image={{ uri: item.imageLink }}
                    title={item.title}
                    viewers={item.viewers}
                    isHome={true}
                  />
                );
              }}
              contentContainerStyle={{ flex: mostViewed.length === 0 ? 1 : null }}
              ListEmptyComponent={
                <View style={{ alignItems: 'center', flex: 1 }}>
                  <Text>No Item in this Category</Text>
                </View>
              }
            />
            <View style={styles.mainCategoryContainer}>
              <View style={styles.categoryContainer}>
                <Text style={styles.categoryText}>Recommended</Text>
              </View>
              <View style={styles.seeAllContainer}>
                <TouchableOpacity
                  onPress={
                    () => navigation.navigate('Recommended', { allRecommended })
                  }
                >
                  <Text style={styles.seeAllText}>See All</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        }
        ListEmptyComponent={
          <View style={{ alignItems: 'center' }}>
            <Text>No Item in this Category</Text>
          </View>
        }
      // ListFooterComponent={
      //   <Text>
      //     An array of objects lets you store multiple values in a single
      //     variable. It stores a fixedsize sequential collection of elements of
      //     the same type. An array is used to store a collection of data,but it
      //     is often more useful to think of an array as a collection of
      //     variables of the same type.
      //   </Text>
      // }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  // mainContainer: {
  //   flex: 1,
  // },
  // flatListContainer: {
  //   padding: 8,
  // },
  // dataContainer: {
  //   margin: 8,
  //   borderColor: '#96ceb4',
  //   borderWidth: 2,
  //   borderRadius: 10,
  //   padding: 16,
  //   flexDirection: 'row',
  // },
  // movieImage: {
  //   width: 130,
  //   height: 200,
  //   borderRadius: 10,
  // },
  // title: {
  //   fontSize: 18,
  //   fontWeight: 'bold',
  // },
  // movieDescriptionContainer: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   marginLeft: 8,
  // },
  // yearContainer: {
  //   marginTop: 8,
  //   marginBottom: 8,
  //   // marginVertical: 8
  // },
  // mainCategoryContainer: {
  //   marginTop: 8,
  //   marginLeft: 8,
  //   marginRight: 8,
  //   flexDirection: 'row',
  // },
  // categoryContainer: {
  //   flex: 1,
  // },
  // categoryText: {
  //   fontSize: 20,
  //   fontWeight: 'bold',
  // },
  // seeAllContainer: {
  //   flex: 1,
  //   alignItems: 'flex-end',
  //   justifyContent: "center"
  // },
  // seeAllText: {
  //   color: '#009688',
  //   textDecorationLine: 'underline'
  // }
});

export default HomeScreen;
