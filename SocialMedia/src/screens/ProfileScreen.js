import React, { useState } from 'react'
import { View, Text, Image, ScrollView, StyleSheet, Modal } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { InputComponent } from '../components/InputComponent'
import { ButtonComponent } from '../components/ButtonComponent'
import { LoginUser } from '../../store/action/ProfileAction'

const ProfileScreen = () => {
  const [isModalVisible, setIsmodalVisible] = useState(false)

  const dispatch = useDispatch()

  const globalProfileData = useSelector(store => store.profileReducer)

  const onLogout = () => {
    setIsmodalVisible(false);
    dispatch(LoginUser(false))
  }
  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.mainContainer}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../assets/images/AvatarMaker1.png')}
          />
        </View>
        <View style={styles.inputContainer}>
          <InputComponent
            title='Username'
            editable={false}
            value={globalProfileData.username}
          />
          <InputComponent
            title='Email'
            editable={false}
            value={globalProfileData.email}
          />
          <InputComponent
            title='Password'
            editable={false}
            value={globalProfileData.password}
          />
        </View>
        <ButtonComponent
          text='Logout'
          isLogout={true}
          onPress={() => setIsmodalVisible(true)}
        />
        <Modal
          animationType='slide'
          transparent={true}
          visible={isModalVisible}
          onRequestClose={() => (
            setIsmodalVisible(!isModalVisible)
          )}
        >
          <View style={styles.backgroundView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>
                Are you sure want to logout
              </Text>
              <View style={styles.modalButton}>
                <ButtonComponent
                  text='Yes'
                  onPress={() => onLogout()}
                />
                <ButtonComponent
                  text='No'
                  isLogout={true}
                  onPress={() => setIsmodalVisible(false)}
                />
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </ScrollView>

  )
}

const styles = StyleSheet.create({
  scroll: {
    flexGrow: 1
  },
  mainContainer: {
    flex: 1,
    alignItems: 'center'
  },
  imageContainer: {
    margin: 16
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: 'black'
  },
  inputContainer: {
    padding: 16,
    width: '100%'
  },
  backgroundView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.8)'
  },
  modalView: {
    margin: 16,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 16,
    alignItems: 'center',
    borderColor: 'black',
    borderWidth: 1,
    width: '80%'
  },
  modalText: {
    marginVertical: 16,
    textAlign: 'center',
    fontSize: 16
  },
  modalButton: {
    flexDirection: 'row'
  }
})

export default ProfileScreen