import React from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'

export const InputComponent = (props) => {
    const { title, isPassword, iconName } = props
    return (
        <View style={styles.mainContainer}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>
                    {title}
                </Text>
            </View>
            <View style={styles.inputContainer}>
                <TextInput
                    style={styles.input}
                    {...props}
                />
                {
                    isPassword ?
                        <View style={styles.iconContainer}>
                            <TouchableOpacity {...props}>
                                <Icon
                                    type='ionicon'
                                    name={iconName}
                                    size={22}
                                />
                            </TouchableOpacity>
                        </View>
                        :
                        null
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        marginBottom: 8,
    },
    titleContainer: {
        marginLeft: 16
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    inputContainer: {
        borderWidth: 1,
        borderRadius: 20,
        margin: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input: {
        padding: 8,
        flex: 1,
        color: 'black'
    },
    iconContainer: {
        padding: 8
    }
})
