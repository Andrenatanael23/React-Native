export const CreateProfile = (value) => {
    return {
        type: 'CREATE_PROFILE',
        payload: value
    }
}

export const LoginUser = (value) => {
    return {
        type: 'LOGIN',
        payload: value
    }
}