import react from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

export const CardComponent = props => {
    const { picture, name, description } = props;
    const openAlert = name => {
        if (name === 'Mercury') {
            alert('You clicked Mercury!');
        } else if (name === 'Venus') {
            alert('You clicked Venus!');
        } else if (name === 'Earth') {
            alert('You clicked Earth!');
        } else if (name === 'Mars') {
            alert('You clicked Mars!');
        } else if (name === 'Jupiter') {
            alert('You clicked Jupiter!');
        } else if (name === 'Saturn') {
            alert('You clicked Saturn!');
        } else if (name === 'Uranus') {
            alert('You clicked Uranus!');
        } else if (name === 'Neptune') {
            alert('You clicked Neptune!');
        }
    };
    return (
        <TouchableOpacity
            style={{
                flexDirection: 'row',
                backgroundColor: 'wheat',
                margin: 16,
                padding: 16,
                borderWidth: 1,
                borderRadius: 20,
                borderColor: 'green',
            }}
            onPress={() => openAlert(name)}>
            <Image style={{ width: 100, height: 100 }} source={picture} />
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    marginLeft: 8,
                    paddingRight: 8,
                }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'black' }}>
                    {name}
                </Text>
                <Text style={{ fontSize: 16, textAlign: 'justify' }}>{description}</Text>
            </View>
        </TouchableOpacity>
    );
};
