import react from "react";
import { View, Text, Image, ImageBackground, TouchableOpacity, ScrollView } from "react-native";
import { CardComponent } from "../components/CardComponent";

const SolarSystemScreen = () => {
    return (
        <View style={{ flex: 1 }}>
            <View style={{ padding: 16, alignItems: 'center' }}>
                <Text style={{
                    textTransform: 'uppercase',
                    fontSize: 30,
                    fontFamily: 'serif',
                    textDecorationLine: 'underline',
                    color: 'black'
                }}>
                    Solar System
                </Text>
            </View>
            <ScrollView>
                <CardComponent
                    picture={require('../../assets/images/mercury.png')}
                    name='Mercury'
                    description='Mercury is the fastest planet, zipping around the sun every 88 earth days'
                />
                <CardComponent
                    picture={require('../../assets/images/venus.png')}
                    name='Venus'
                    description='Venus spins slowly in the opposite direction from most planets'
                />
                <CardComponent
                    picture={require('../../assets/images/earth.png')}
                    name='Earth'
                    description="Earth is the only place we know sofar that's inhabited by living things"
                />
                <CardComponent
                    picture={require('../../assets/images/mars.png')}
                    name='Mars'
                    description='Mars is dusty, cold, desert world with a very thin atmosphere'
                />
                <CardComponent
                    picture={require('../../assets/images/jupiter.png')}
                    name='Jupiter'
                    description='Jupiter is more than twice as massive than the other planets of our solar system combined'
                />
                <CardComponent
                    picture={require('../../assets/images/saturn.png')}
                    name='Saturn'
                    description='Adorned with a dazzling, complex system of icy rings, Saaturn is unique in our solar system'
                />
                <CardComponent
                    picture={require('../../assets/images/uranus.png')}
                    name='Uranus'
                    description='Uranus rotates at nearly 90-degree angle from the plane of its orbit'
                />
                <CardComponent
                    picture={require('../../assets/images/neptune.png')}
                    name='Neptune'
                    description='Neptune is dark, cold, and whipped by supersonic winds'
                />
            </ScrollView>
        </View>
    )
};

export default SolarSystemScreen;