import React, { useEffect } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { SongComponent } from '../components/SongComponent'


const DetailScreen = (props) => {
    const { route } = props
    const song = route.params.item

    useEffect(() => {
        // console.log(song)
    }, [])
    return (
        <View style={styles.mainContainer}>
            <Image
                style={styles.songImage}
                source={{ uri: song.imageLink }}
            />
            <View style={styles.titleContainer}>
                <Text style={styles.title}>
                    {song.title}
                </Text>
            </View>
            <SongComponent
                name='Singer'
                value={song.singer}
            />
            <SongComponent
                name='Year'
                value={song.year}
            />
            <SongComponent
                name='Genre'
                value={song.genre}
            />
            <SongComponent
                name='Songwriters'
                value={song.songwriters}
            />
            <SongComponent
                name='Rating'
                isRating={true}
                rating={song.rating}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'center',
        margin: 16
    },
    songImage: {
        width: 200,
        height: 200,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: 'salmon'
    },
    titleContainer: {
        marginTop: 8,
        marginBottom: 16
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold'
    }

})

export default DetailScreen