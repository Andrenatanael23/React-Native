import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native'
import { songData } from '../../data/SongData';

const HomeScreen = (props) => {
    const { navigation } = props;
    const [recommended, setRecommended] = useState([]);

    const compareRating = (a, b) => {
        const ratingA = a.rating;
        const ratingB = b.rating;

        if (ratingA > ratingB) {
            return -1;
        } else if (ratingA < ratingB) {
            return 1;
        } else {
            return 0;
        }
    };

    useEffect(() => {
        const sortedRecommended = [...songData].sort(compareRating);
        setRecommended(sortedRecommended);
    }, []);

    return (
        <View style={styles.mainContainer}>
            <FlatList
                data={recommended}
                keyExtractor={(item) => item.id}
                contentContainerStyle={styles.flatListContainer}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.dataContainer}>
                            <Image
                                style={styles.songImage}
                                source={{ uri: item.imageLink }}
                            />
                            <View style={styles.descriptionContainer}>
                                <Text style={styles.title}>
                                    {item.title}
                                </Text>
                                <View style={styles.singerContainer}>
                                    <Text style={styles.singer}>
                                        {item.singer}
                                    </Text>
                                </View>
                                {/* <Text style={styles.rating}>
                                    {item.rating}
                                </Text> */}
                                {item.rating === 5 ? (
                                    <Image
                                        style={styles.ratingImage}
                                        source={require('../../assets/images/five-stars.png')}
                                    />
                                ) : item.rating === 4 ? (
                                    <Image
                                        style={styles.ratingImage}
                                        source={require('../../assets/images/four-stars.png')}
                                    />
                                ) : item.rating === 3 ? (
                                    <Image
                                        style={styles.ratingImage}
                                        source={require('../../assets/images/three-stars.png')}
                                    />
                                ) : item.rating === 2 ? (
                                    <Image
                                        style={styles.ratingImage}
                                        source={require('../../assets/images/two-stars.png')}
                                    />
                                ) : (
                                    <Image
                                        style={styles.ratingImage}
                                        source={require('../../assets/images/one-star.png')}
                                    />
                                )}
                                <View style={styles.buttonContainer}>
                                    <TouchableOpacity
                                        style={styles.seeDetailsButton}
                                        onPress={() => navigation.navigate('Detail', { item })}
                                    >
                                        <Text style={styles.seeDetailsText}>
                                            See Details
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    flatListContainer: {
        padding: 8,
    },
    dataContainer: {
        margin: 8,
        borderWidth: 2,
        borderBlockColor: '#A6A6BD',
        padding: 8,
        flexDirection: 'row',
        alignItems: 'center'
    },
    songImage: {
        width: 120,
        height: 120
    },
    descriptionContainer: {
        paddingLeft: 8,
        flex: 1
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    singerContainer: {
        marginVertical: 8,
    },
    ratingImage: {
        width: 100,
        height: 20
    },
    buttonContainer: {
        alignItems: 'baseline',
        marginTop: 8
    },
    seeDetailsButton: {
        padding: 8,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#A6A6BD',
        backgroundColor: '#AFCAE8'
    },
    seeDetailsText: {
        color: 'white'
    }


})

export default HomeScreen