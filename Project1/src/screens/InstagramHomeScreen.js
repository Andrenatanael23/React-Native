import react from "react";
import { View, Text } from "react-native";
import { Icon } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import { Feed, Story } from "../components/InstagramComponen";

const InstagramHomeScreen = () => {
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
                <Text style={{
                    fontFamily: 'StyleScript-Regular',
                    fontSize: 40,
                    marginLeft: 16,
                    color: 'black'
                }}>
                    Instagram
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 8 }}>
                    <View style={{ margin: 8 }}>
                        <Icon
                            name='plus-square-o'
                            type='font-awesome'
                            size={26}
                        />
                    </View>
                    <View style={{ margin: 8 }}>
                        <Icon
                            name='heart-o'
                            type='font-awesome'
                            size={26}
                        />
                    </View>
                    <View style={{ margin: 8 }}>
                        <Icon
                            name='paper-plane-outline'
                            type='ionicon'
                            size={26}
                        />
                    </View>
                </View>
            </View>
            <ScrollView>
                <View style={{ flexDirection: 'row', margin: 8}}>
                    <ScrollView horizontal>
                        <Story 
                            profile={require('../../assets/images/profilePicture1.png')}
                            username="User1"
                        />
                        <Story 
                            profile={require('../../assets/images/profilePicture2.png')}
                            username="User2"
                        />
                        <Story 
                            profile={require('../../assets/images/profilePicture3.png')}
                            username="User3"
                        />
                        <Story 
                            profile={require('../../assets/images/profilePicture4.png')}
                            username="User4"
                        />
                        <Story 
                           profile={require('../../assets/images/profilePicture5.png')}
                           username="User5"
                        />
                    </ScrollView>
                </View>
                <Feed
                    profile={require('../../assets/images/profilePicture1.png')}
                    username="User1"
                    post={{ uri: "https://wallpaperaccess.com/full/201215.jpg" }}
                />
                <Feed
                    profile={require('../../assets/images/profilePicture2.png')}
                    username="User2"
                    post={{ uri: "https://images4.alphacoders.com/292/292026.jpg" }}
                />
                <Feed
                    profile={require('../../assets/images/profilePicture3.png')}
                    username="User3"
                    post={{ uri: "https://wallpaperaccess.com/full/496881.jpg" }}
                />
                <Feed
                    profile={require('../../assets/images/profilePicture4.png')}
                    username="User4"
                    post={{ uri: "https://images5.alphacoders.com/316/316297.jpg" }}
                />
                <Feed
                    profile={require('../../assets/images/profilePicture5.png')}
                    username="User5"
                    post={{ uri: "https://wallpaperaccess.com/full/1403923.jpg" }}
                />
            </ScrollView>
        </View>
    )
}

export default InstagramHomeScreen;