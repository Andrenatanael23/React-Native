import react from 'react';
import { View, Text, ImageBackground, TextInput } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { InputComponent } from '../components/InputComponent';

const FourthScreen = () => {
    return (
        <View style={{ margin: 10 }}>
            <ScrollView>
                <ImageBackground
                    style={{
                        width: 370,
                        height: 300,
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                        opacity: 0.5,
                    }}
                    imageStyle={{ borderRadius: 10 }}
                    source={{ uri: 'https://wallpaperaccess.com/full/250180.jpg' }}>
                    <Text
                        style={{
                            backgroundColor: 'mistyrose',
                            padding: 8,
                            margin: 8,
                            fontSize: 18,
                            fontWeight: 'bold',
                        }}>
                        Disneyland
                    </Text>
                </ImageBackground>
                <InputComponent
                    title="Description"
                    height={140}
                    placeholder="About the place"
                    multiline={true}
                    keyboardType="default"
                />
                <InputComponent
                    title="Phone Number"
                    height={40}
                    placeholder="Phone Number"
                    multiline={true}
                    keyboardType="phone-pad"
                />
                <InputComponent
                    title="Location"
                    height={40}
                    placeholder="Location"
                    multiline={true}
                    keyboardType="default"
                />
            </ScrollView>
        </View>
    );
};

const ChallengeScreen = () => {
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                style={{ flex: 1, justifyContent: 'center' }}
                imageStyle={{ opacity: 0.5 }}
                source={{ uri: 'https://i.pinimg.com/736x/e4/28/c5/e428c5f6e045bcf567fa4267f7985076.jpg' }}
            >
                <View style={{ alignItems: 'center' }}>
                    <Text style={{
                        fontWeight: 'bold',
                        fontSize: 18,
                        backgroundColor: 'beige',
                        padding: 10,
                        margin: 10
                    }}>
                        Colosseum
                    </Text>
                </View>
                <View style={{ margin: 8, padding: 8 }}>
                    <InputComponent
                        title="Description"
                        height={140}
                        placeholder="About the place"
                        multiline={true}
                        keyboardType="default"
                    />
                    <InputComponent
                        title="Phone Number"
                        height={40}
                        placeholder="Phone Number"
                        multiline={true}
                        keyboardType="phone-pad"
                    />
                    <InputComponent
                        title="Location"
                        height={40}
                        placeholder="Location"
                        multiline={true}
                        keyboardType="default"
                    />
                </View>
            </ImageBackground>
        </View>
    )
}

export default ChallengeScreen;
