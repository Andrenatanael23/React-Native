import react from "react";
import { View, Text, Button } from "react-native";

const FirstScreen = () => {
    const openAlert = () => {
        alert('You clicked the button!')
    }
    return (
        <View style = {{
            flex:1,
            alignItems:'center',
            justifyContent:'center'
        }}>
            <Text style = {{margin:16}}> 
                Hello
                <Text style = {{color:'red'}}>
                    World!
                </Text>
            </Text>
            <Button title="This is button" onPress={openAlert}/>
        </View>
    )
};

const ChallengeScreen = () => {
    const openAlert = () => {
        alert('You are awesome!')
    }
    return (
        <View style = {{
            flex:1,
            alignItems:'center',
            justifyContent:'center'
        }}>
            <Text style = {{
                color:'green',
                margin:16
            }}>
                Let's Learn React Native Framework!
            </Text>
            <Text>
                My name is Andre
            </Text>
            <Text style =  {{margin:16}}>
                Now i learn about
                <Text style = {{color:'blue'}}>
                     React Native Components
                </Text>
                 to build user interface for android apps
            </Text>
            <Text>
                I Love Coding!
            </Text>
            <Button title="CLICK ME" onPress={openAlert}/>
        </View>
    )
}

export default FirstScreen;