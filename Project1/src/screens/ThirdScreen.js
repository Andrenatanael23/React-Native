import react from 'react';
import { View, Text, Image, ScrollView } from 'react-native';

const ThirdScreen = () => {
    return (
        <View style={{
            backgroundColor: 'mistyrose',
            flex: 1,
            flexDirection: 'row'
        }}>
            <View style={{ backgroundColor: 'red', flex: 1 }}>
                <Text style={{ color: 'white' }}>
                    Pikachu
                </Text>
            </View>
            <View style={{ backgroundColor: 'orange', flex: 1 }}>
                <Text>
                    Flareon
                </Text>
            </View>
            <View style={{ backgroundColor: 'yellow', flex: 1 }}>
                <Text>
                    Rapidash
                </Text>
            </View>
        </View>
    )
}

const AddImage = () => {
    return (
        <ScrollView>
            <View style={{ alignItems: 'center' }}>
                <View style={{ margin: 8, padding: 8 }}>
                    <Image style={{
                        width: 150,
                        height: 150,
                        borderRadius: 100,
                        borderWidth: 1,
                        borderColor: 'green',
                        backgroundColor: 'aliceblue'
                    }}
                        source={require('../../assets/images/pikachu.png')}
                    />
                </View>
                <View style={{ margin: 8, padding: 8 }}>
                    <Image style={{
                        width: 150,
                        height: 150,
                        borderRadius: 100,
                        borderWidth: 1,
                        borderColor: 'green',
                        backgroundColor: 'aliceblue'
                    }}
                        source={require('../../assets/images/flareon.png')}
                    />
                </View>
                <View style={{ margin: 8, padding: 8 }}>
                    <Image style={{
                        width: 150,
                        height: 150,
                        borderRadius: 100,
                        borderWidth: 1,
                        borderColor: 'green',
                        backgroundColor: 'aliceblue'
                    }}
                        source={require('../../assets/images/rapidash.png')}
                    />
                </View>
                <View style={{ margin: 8, padding: 8 }}>
                    <Image style={{
                        width: 150,
                        height: 150,
                        borderRadius: 100,
                        borderWidth: 1,
                        borderColor: 'green',
                        backgroundColor: 'aliceblue'
                    }}
                        source={{ uri: 'https://i.ibb.co/bLz74hj/psyduck.png' }}
                    />
                </View>
                <View style={{ margin: 8, padding: 8 }}>
                    <Image style={{
                        width: 150,
                        height: 150,
                        borderRadius: 100,
                        borderWidth: 1,
                        borderColor: 'green',
                        backgroundColor: 'aliceblue'
                    }}
                        source={{ uri: 'https://i.ibb.co/GQBLC7r/bulbasaur.png' }}
                    />
                </View>
            </View>
        </ScrollView>
    )
}

const ChallengeScreen = () => {
    return (
        <View>
            <View style={{ alignItems: 'center', margin: 8 }}>
                <Text style={{ color: 'blue', fontWeight: 'bold' }}>Pokemon's Profile</Text>
            </View>
            <ScrollView style={{marginBottom:30}}>
                <View style={{ margin: 8, padding: 8, flexDirection: 'row' }}>
                    <ScrollView horizontal={true}>
                        <Image style={{
                            width: 150,
                            height: 150,
                            borderRadius: 10,
                            borderWidth: 1,
                            borderColor: 'green',
                            backgroundColor: 'aliceblue'
                        }}
                            source={require('../../assets/images/pikachu.png')}
                        />
                        <View style={{ marginLeft: 8, justifyContent: 'space-evenly' }}>
                            <Text>
                                Name : Pikachu {'\n'}
                                Category : Electric {'\n'}
                                Abilities : Static {'\n'}
                                Weakness : Ground {'\n'}
                            </Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{ margin: 8, padding: 8, flexDirection: 'row' }}>
                    <ScrollView horizontal={true}>
                        <Image style={{
                            width: 150,
                            height: 150,
                            borderRadius: 10,
                            borderWidth: 1,
                            borderColor: 'green',
                            backgroundColor: 'aliceblue'
                        }}
                            source={require('../../assets/images/flareon.png')}
                        />
                        <View style={{ marginLeft: 8, justifyContent: 'space-evenly' }}>
                            <Text>
                                Name : Flareon {'\n'}
                                Category : Fire {'\n'}
                                Abilities : Flash Fire {'\n'}
                                Weakness : Water, Ground, Rock {'\n'}
                            </Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{ margin: 8, padding: 8, flexDirection: 'row' }}>
                    <ScrollView horizontal={true}>
                        <Image style={{
                            width: 150,
                            height: 150,
                            borderRadius: 10,
                            borderWidth: 1,
                            borderColor: 'green',
                            backgroundColor: 'aliceblue'
                        }}
                            source={require('../../assets/images/rapidash.png')}
                        />
                        <View style={{ marginLeft: 8, justifyContent: 'space-evenly' }}>
                            <Text>
                                Name : Rapidash {'\n'}
                                Category : Fire {'\n'}
                                Abilities : Run Away, Flash Fire {'\n'}
                                Weakness : Water, Ground, Rock {'\n'}
                            </Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{ margin: 8, padding: 8, flexDirection: 'row' }}>
                    <ScrollView horizontal={true}>
                        <Image style={{
                            width: 150,
                            height: 150,
                            borderRadius: 10,
                            borderWidth: 1,
                            borderColor: 'green',
                            backgroundColor: 'aliceblue'
                        }}
                            source={{ uri: 'https://i.ibb.co/bLz74hj/psyduck.png' }}
                        />
                        <View style={{ marginLeft: 8, justifyContent: 'space-evenly' }}>
                            <Text>
                                Name : Psyduck {'\n'}
                                Category : Water {'\n'}
                                Abilities : Dump, Cloud Nine {'\n'}
                                Weakness : Grass, Electric {'\n'}
                            </Text>
                        </View>
                    </ScrollView>
                </View>
                <View style={{ margin: 8, padding: 8, flexDirection: 'row' }}>
                    <ScrollView horizontal={true}>
                        <Image style={{
                            width: 150,
                            height: 150,
                            borderRadius: 10,
                            borderWidth: 1,
                            borderColor: 'green',
                            backgroundColor: 'aliceblue'
                        }}
                            source={{ uri: 'https://i.ibb.co/GQBLC7r/bulbasaur.png' }}
                        />
                        <View style={{ marginLeft: 8, justifyContent: 'space-evenly' }}>
                            <Text>
                                Name : Bulbasaur {'\n'}
                                Category : Grass {'\n'}
                                Abilities : Overgrow {'\n'}
                                Weakness : Fire, Physic, Flying, Ice {'\n'}
                            </Text>
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        </View>
    )
}

export default ThirdScreen;