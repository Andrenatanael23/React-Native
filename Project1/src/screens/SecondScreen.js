import react from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const SecondScreen = () => {
    const openAlert = game => {
        if (game === 'mobileLegends') {
            alert('You chose Mobile Legends!');
        } else if (game === 'pubg') {
            alert('You chose PUBG!');
        }
    };
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: 'lavender',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
            <View
                style={{
                    backgroundColor: 'skyblue',
                    margin: 10,
                    padding: 10,
                    borderWidth: 2,
                    borderColor: 'red',
                    borderStyle: 'dashed',
                    borderRadius: 1,
                }}>
                <Text
                    style={{
                        margin: 5,
                        textAlign: 'center',
                        textTransform: 'uppercase',
                        textDecorationLine: 'underline',
                        fontSize: 16,
                        fontWeight: 'bold',
                    }}>
                    Mobile Legends
                </Text>
                <Text>
                    <Text
                        style={{
                            color: 'purple',
                            fontWeight: 'bold',
                        }}>
                        Mobile Legends
                    </Text>{' '}
                    is a multiplayer online battle arena (MOBA) game. The two opposing
                    teams fight to reach and destroy the enemy's base while defending
                    their own base for control of a path.
                </Text>
            </View>
            <View>
                <TouchableOpacity
                    style={{
                        backgroundColor: 'white',
                        margin: 8,
                        padding: 8,
                        borderRadius: 50,
                        borderWidth: 2,
                    }}
                    onPress={() => openAlert('mobileLegends')}>
                    <Text style={{ color: 'lightcoral' }}>Mobile Legends Button</Text>
                </TouchableOpacity>
            </View>
            <View
                style={{
                    backgroundColor: 'cornflowerblue',
                    margin: 10,
                    padding: 10,
                    borderWidth: 2,
                    borderColor: 'red',
                    borderStyle: 'dotted',
                    borderRadius: 1,
                }}>
                <Text
                    style={{
                        margin: 5,
                        textAlign: 'center',
                        textTransform: 'uppercase',
                        textDecorationLine: 'underline',
                        fontSize: 16,
                        fontWeight: 'bold',
                    }}>
                    PUBG
                </Text>
                <Text>
                    <Text
                        style={{
                            color: 'purple',
                            fontWeight: 'bold',
                        }}>
                        PlayerUnknown's Battlegrounds,
                    </Text>{' '}
                    better known as PUBG, is a multiplayer battle royale game in which
                    players drop onto an island and fight to be the last one left
                    standing.
                </Text>
            </View>
            <View>
                <TouchableOpacity
                    style={{
                        backgroundColor: 'white',
                        margin: 8,
                        padding: 8,
                        borderRadius: 50,
                        borderWidth: 2,
                    }}
                    onPress={() => openAlert('pubg')}>
                    <Text style={{ color: 'lightcoral' }}>PUBG Button</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const ChallengeScreen = () => {
    return (
        <View
            style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
            <View>
                <Text style={{
                    margin:10,
                    color: 'blue',
                    textDecorationLine: 'underline',
                    textTransform: 'uppercase',
                    fontWeight: 'bold',
                    fontSize: 16
                }}>iOS</Text>
            </View>
            <View style={{
                margin:10,
                padding:10,
                borderStyle:'dotted',
                borderWidth:2,
                borderRadius:20,
                backgroundColor:'#FFDEAD'
            }}>
                <Text style={{textAlign:'justify'}}>
                    iOS (formerly iPhone OS) is a mobile operating system created and
                    developed by Apple Inc. exclusively for its hardware. It is the
                    operating system that powers many of the company's mobile devices,
                    including the iPhone and iPod Touch.
                </Text>
            </View>
            <View>
                <Text style={{margin:10, fontWeight:'bold'}}>VS.</Text>
            </View>
            <View>
                <Text style={{
                    margin:10,
                    color: 'green',
                    textDecorationLine: 'underline',
                    textTransform: 'uppercase',
                    fontWeight: 'bold',
                    fontSize: 16
                }}>Android</Text>
            </View>
            <View style={{
                margin:10,
                padding:10,
                borderStyle:'dotted',
                borderWidth:2,
                borderRadius:20,
                backgroundColor:'#FFB7D5'
            }}>
                <Text style={{textAlign:'justify'}}>
                    <Text style={{fontWeight:'bold'}}>Android</Text> is a mobile operating system based 
                    on a modified version of the Linux kernel and other open source software,
                    designed primarily for touchscreen mobile devices such as smartphones and tablets.
                    Android is developed by a consortium of developers known as the{' '}
                    <Text style={{color:'purple', fontStyle:'italic', fontWeight:'bold'}}>Open Handset Alliance</Text>{' '} 
                    and commercially sponsored by <Text style={{color:'blue', fontStyle:'italic', fontWeight:'bold'}}>Google</Text>.
                </Text>
            </View>
        </View>
    );
};

export default SecondScreen;
