import react from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";
import ThirdScreen from "./src/screens/ThirdScreen";
const App = () => {
  return (
    <SafeAreaProvider>
      <ThirdScreen />
    </SafeAreaProvider>
  )
}

export default App;