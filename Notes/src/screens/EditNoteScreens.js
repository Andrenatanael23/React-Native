import { View, Text, StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'
import { HeaderComponent, MainComponent } from '../components/NoteComponent'
import realm from '../../store/realm'

const EditNoteScreens = (props) => {
    const { route, navigation } = props
    const id = route.params.id
    const [dataToUpdate, setDataToUpdate] = useState([])

    // useEffect(() => {
    //     console.log(id + ' id on edit screen')
    // }, [id])

    useEffect(() => {
        const data = realm.objects('Note').filtered(`id = ${id}`)
        setDataToUpdate(data)
    }, [])

    useEffect(() => {
        console.log('edit screen')
        console.log(dataToUpdate)
    }, [dataToUpdate])

    return (
        <View style={styles.mainContainer}>
            <HeaderComponent
                title='Edit'
                onPress={() => saveNote(
                    isEdit ? newNote : dataToUpdate[0].note
                )}
            />
            <MainComponent
                date="Date"
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
})
export default EditNoteScreens