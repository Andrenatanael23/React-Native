import { View, Text, TextInput, TouchableOpacity, StyleSheet, Alert } from 'react-native'
import React, { useState } from 'react'
import { Icon } from 'react-native-elements'
import realm from '../../store/realm'
import { HeaderComponent, MainComponent } from '../components/NoteComponent'

const AddNoteScreen = (props) => {
    const { navigation } = props;
    const [tempNote, setTempNote] = useState('');

    const getCurrentDate = () => {
        const months = [
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ];
        const noteDate = new Date();
        const dateOnly = noteDate.getDate()
        const monthOnly = noteDate.getMonth();
        const yearOnly = noteDate.getFullYear();

        return months[monthOnly] + ' ' + dateOnly + ', ' + yearOnly;
    }

    const saveNote = (newNote) => {
        const allData = realm.objects('Note')
        const dataLength = allData.length
        let lastIdFromRealm = 0

        if (dataLength !== 0) {
            lastIdFromRealm = allData[dataLength - 1].id
        }

        if (newNote !== '') {
            realm.write(() => {
                realm.create("Note", {
                    id: allData == 0 ? 1 : lastIdFromRealm + 1,
                    note: newNote,
                    date: new Date().toISOString()
                })
            })
            Alert.alert(
                "Success",
                "Successfully Added New Note",
                [
                    {
                        text: "OK",
                        onPress: () => navigation.navigate('NoteList')
                    }
                ]
            )
            const data = realm.objects('Note')
            console.log(data)

        } else {
            alert('Empty Note')
        }
    }
    return (
        <View style={styles.mainContainer}>
            {/* <View style={styles.headerContainer}>
                <Text style={styles.title}>Create</Text>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => saveNote(tempNote)}
                >
                    <Icon
                        name="check"
                        type="font-awesome-5"
                        size={18}
                    />
                </TouchableOpacity>
            </View> */}
            {/* <Text style={styles.date}>{getCurrentDate()}</Text>
            <TextInput
                multiline
                placeholder='Write Here'
                style={styles.input}
                onChangeText={(text) => setTempNote(text)}
            /> */}
            <HeaderComponent
                title='Create'
                onPress={() => saveNote(tempNote)}
            />
            <MainComponent
                date={getCurrentDate()}
                onChangeText={(text) => setTempNote(text)}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    headerContainer: {
        padding: 8,
        backgroundColor: 'moccasin',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    title: {
        padding: 8,
        fontSize: 20,
        fontWeight: 'bold'
    },
    button: {
        padding: 8
    },
    date: {
        paddingTop: 16,
        paddingLeft: 16
    },
    input: {
        fontSize: 16,
        flex: 1,
        paddingHorizontal: 16,
        textAlignVertical: 'top'
    }
})

export default AddNoteScreen;